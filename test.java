import java.util.Arrays;
import java.util.List;

import io.restassured.RestAssured;
import io.restassured.mapper.ObjectMapper;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestPojo {

	public static void main(String[] args) {
		RestAssured.baseURI = "https://api.openweathermap.org/data/2.5/weather";
		RequestSpecification httpReq = RestAssured.given();
		Response response = httpReq.get(
				"https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=2fab385f827514edd8a752ff74c3f268");
		System.out.println(response.asString());
		Example wMap= response.as(WeatherMap.class,ObjectMapperType.GSON);
		List<Main> list1 = Arrays.asList(wMap.getCod());
		for(Main c1:list1) {
			System.out.println(c1.getHumidity());
		}
	}
}